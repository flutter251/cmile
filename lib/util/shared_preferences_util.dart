

import 'package:shared_preferences/shared_preferences.dart';

class SharedPref{

    static Future<void> setValue(String key, String value) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString(key, value);
    }

    static dynamic getValue(String key) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      return prefs.getString(key);
    }

    static dynamic removeValue(String key) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      final success = await prefs.remove('counter');
      return success;
    }
}