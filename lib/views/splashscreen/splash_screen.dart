import 'dart:async';

import 'package:cmile_coding_assignment/repository/repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  bool _isLoggedIn = false;
  late AnimationController animationController;
  late Animation<double> animation;

  @override
  void initState() {
    super.initState();
    startTime();

    animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2),
    )..addListener(() => setState(() {}));
    animation = CurvedAnimation(
      parent: animationController,
      curve: Curves.easeInOut,
    );
    animationController.forward();
  }

  startTime() async {
    var _duration = const Duration(milliseconds: 3000);
    return Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Repository().getUserLoggedIn().then((bool isLoggedIn) {
      _isLoggedIn = isLoggedIn;
      print("is user LoggedIn $_isLoggedIn");
      if (_isLoggedIn) {
        Navigator.pushReplacementNamed(context, '/DashboardScreen');
      } else {
        Navigator.pushReplacementNamed(context, '/LoginScreen');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          color: const Color(0xffE8F8DC),
          width: MediaQuery.of(context).size.width,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ScaleTransition(
                  scale: animation,
                  child: Text(
                    "Cmile Assignment",
                    style: TextStyle(
                      fontSize: 24,
                      color: Colors.green[800],
                    ),
                  ),
                ),
              ]),
        ),
      ),
    );
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }
}
