import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cmile_coding_assignment/repository/repository.dart';
import 'package:cmile_coding_assignment/repository/repository.dart';
import 'package:meta/meta.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  Repository repository;

  LoginBloc({required this.repository}) : super(LoginInitial());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is DoLogin) {
      bool isUserLoggedIn = await Repository().setUserLoggedIn();
      yield LoginResponse(isUserLoggedIn: isUserLoggedIn);
    }
  }

}
