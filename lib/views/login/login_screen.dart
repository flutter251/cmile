import 'package:cmile_coding_assignment/di/injection_container.dart';
import 'package:cmile_coding_assignment/util/text_fields.dart';
import 'package:cmile_coding_assignment/util/text_input_fields.dart';
import 'package:cmile_coding_assignment/views/dashboard/dashboard_screen.dart';
import 'package:cmile_coding_assignment/views/login/login_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late TextEditingController _phoneController;
  final LoginBloc _bloc = injector<LoginBloc>();

  @override
  void initState() {
    super.initState();
    _phoneController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: BlocProvider(
          create: (BuildContext context) => _bloc,
          child: BlocListener(
            bloc: _bloc,
            listener: (BuildContext context, LoginState state) {
              if (state is LoginInitial) {
                const CircularProgressIndicator();
              }

              if (state is LoginResponse) {
                if (state.isUserLoggedIn) {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const DashboardScreen()));
                  _phoneController.clear();
                }
              }
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Padding(
                  padding: EdgeInsets.only(left: 35, right: 35),
                  child: TextFields(
                      text: "Welcome",
                      size: 33.0,
                      color: Color.fromRGBO(85, 85, 85, 1.0),
                      alignment: Alignment.centerLeft),
                ),
                const SizedBox(height: 23),
                Padding(
                  padding: const EdgeInsets.only(left: 35, right: 35),
                  child: TextInputFields(
                      label: "Phone Number", fieldController: _phoneController),
                ),
                const SizedBox(height: 17),
                ElevatedButton(
                  onPressed: () async {
                    String pattern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
                    RegExp regExp = RegExp(pattern);

                    if (_phoneController.text.isNotEmpty &&
                        _phoneController.text.length == 10 &&
                        regExp.hasMatch(_phoneController.text)) {
                      if (_phoneController.text.startsWith("9") ||
                          _phoneController.text.startsWith("8") ||
                          _phoneController.text.startsWith("7") ||
                          _phoneController.text.startsWith("6")) {
                        _bloc.add(DoLogin());
                      }
                    } else {
                      //showToast()
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                        content: Text("Enter valid Mobile Number"),
                      ));
                      print("Enter valid Mobile Number");
                    }
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.green,
                    padding: const EdgeInsets.symmetric(
                      horizontal: 30,
                      vertical: 15,
                    ),
                    textStyle: const TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  child: const Text(
                    "Submit",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color.fromRGBO(255, 255, 255, 1),
                      fontSize: 18,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
