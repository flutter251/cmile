part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class DoLogin extends LoginEvent{
  DoLogin();
}
