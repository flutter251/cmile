part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}

class LoginResponse extends LoginState {
  final bool isUserLoggedIn;
  LoginResponse({required this.isUserLoggedIn});
}
