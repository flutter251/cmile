import 'dart:async';
import 'package:cmile_coding_assignment/repository/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

part 'dashboard_event.dart';

part 'dashboard_state.dart';

class DashboardBloc extends Bloc<DashboardEvent, DashboardState> {
  Repository repository;
  DashboardBloc({required this.repository}) : super(DashboardInitial());


  @override
  Stream<DashboardState> mapEventToState(DashboardEvent event) async* {
    if (event is EditEmployeeData) {
      yield OnEditState(docId: event.docId, name: event.name, designation: event.designation);
    }

    if (event is DeleteEmployeeData) {
      bool isDeleteSuccessful = await Repository().deleteEmployee(docId: event.docId);
      yield OnDeleteState(isDeleteSuccessful: isDeleteSuccessful);
    }
  }
}
