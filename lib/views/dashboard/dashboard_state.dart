part of 'dashboard_bloc.dart';

@immutable
abstract class DashboardState {}

class DashboardInitial extends DashboardState {}

class OnEditState extends DashboardState {
  final String docId;
  final String name;
  final String designation;
  OnEditState({required this.docId, required this.name,required this.designation});
}

class OnDeleteState extends DashboardState {
  final bool isDeleteSuccessful;
  OnDeleteState({required this.isDeleteSuccessful});
}
