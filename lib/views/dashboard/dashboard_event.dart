part of 'dashboard_bloc.dart';

@immutable
abstract class DashboardEvent {}

class EditEmployeeData extends DashboardEvent {
  final String docId;
  final String name;
  final String designation;

  EditEmployeeData(
      {required this.docId, required this.name, required this.designation});
}

class DeleteEmployeeData extends DashboardEvent {
  final String docId;

  DeleteEmployeeData({required this.docId});
}
