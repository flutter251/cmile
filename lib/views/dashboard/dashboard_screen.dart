import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cmile_coding_assignment/di/injection_container.dart';
import 'package:cmile_coding_assignment/views/edit/employee_edit_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'dashboard_bloc.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  late FirebaseFirestore _firebaseFireStore;
  final DashboardBloc _bloc = injector<DashboardBloc>();

  @override
  void initState() {
    super.initState();
    _firebaseFireStore = FirebaseFirestore.instance;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: BlocProvider(
          create: (BuildContext context) => _bloc,
          child: BlocListener(
            bloc: _bloc,
            listener: (BuildContext context, state) {
              if (state is OnEditState) {
                setState(() {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => EditEmployeeScreen(
                                docId: state.docId,
                                name: state.name,
                                designation: state.designation,
                              )));
                });
              }
              if (state is OnDeleteState) {
                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                  content: Text("Employee Deleted"),
                ));
              }
            },
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(height: 40),
                  const Text(
                    "Employee Data",
                    style: TextStyle(
                        fontSize: 32,
                        color: Colors.green,
                        fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 40),
                  Flexible(
                    child: StreamBuilder<QuerySnapshot>(
                        stream: _firebaseFireStore
                            .collection('empData')
                            .snapshots(),
                        builder: (BuildContext context,
                            AsyncSnapshot<QuerySnapshot> snapshot) {
                          if (snapshot.hasData) {
                            return ListView(
                              children: showEmployeeData(snapshot),
                            );
                          }
                          return const Center(
                              child: Text("There is no employee"));
                        }),
                  ),
                  const SizedBox(height: 40),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  showEmployeeData(AsyncSnapshot<QuerySnapshot> snapshot) {
    return snapshot.data!.docs
        .map((doc) => Card(
              elevation: 5,
              margin: const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
              child: Container(
                width: double.infinity,
                color: Colors.green[50],
                padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
                child: Column(
                  children: [
                    Text(
                      doc["empName"],
                      maxLines: 5,
                      textAlign: TextAlign.start,
                      style: const TextStyle(
                          fontSize: 16,
                          color: Colors.green,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(height: 5),
                    Text(
                      doc["designation"],
                      maxLines: 5,
                      textAlign: TextAlign.start,
                      style: const TextStyle(
                          fontSize: 14,
                          color: Colors.black54,
                          fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        TextButton(
                            onPressed: () {
                              _bloc.add(EditEmployeeData(docId: doc.id, name: doc["empName"], designation:  doc["designation"] ));
                            },
                            child: Text(
                              "Edit",
                              style: TextStyle(color: Colors.green[800]),
                            )),
                        const SizedBox(width: 20),
                        TextButton(
                            onPressed: () async {
                              _bloc.add(DeleteEmployeeData(docId: doc.id));
                            },
                            child: Text("Delete",
                                style: TextStyle(color: Colors.green[800]))),
                      ],
                    )
                  ],
                ),
              ),
            ))
        .toList();
  }
}
