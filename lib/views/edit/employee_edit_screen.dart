import 'package:cmile_coding_assignment/di/injection_container.dart';
import 'package:cmile_coding_assignment/views/dashboard/dashboard_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'employee_edit_bloc.dart';

class EditEmployeeScreen extends StatefulWidget {
  final String docId;
  final String name;
  final String designation;

  const EditEmployeeScreen(
      {required this.docId,
      required this.name,
      required this.designation,
      Key? key})
      : super(key: key);

  @override
  _EditEmployeeScreenState createState() => _EditEmployeeScreenState();
}

class _EditEmployeeScreenState extends State<EditEmployeeScreen> {
  var _formKey;
  late TextEditingController _nameController;
  late TextEditingController _designationController;
  final EmployeeEditBloc _bloc = injector<EmployeeEditBloc>();

  @override
  initState() {
    super.initState();
    _formKey = GlobalKey<FormState>();
    _nameController = TextEditingController();
    _designationController = TextEditingController();

    _nameController.text= widget.name;
    _designationController.text = widget.designation;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return Future.value(false);
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green[700],
          title: const Text("Edit Profile"),
          centerTitle: true,
        ),
        body: BlocProvider(
          create: (BuildContext context) => _bloc,
          child: BlocListener(
            bloc: _bloc,
            listener: (BuildContext context, state) {
              if (state is OnEditState) {
                setState(() {
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    content: Text("Employee Edited"),
                  ));
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const DashboardScreen()));
                });
              }
            },
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(height: 20),
                    Form(
                      key: _formKey,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 30, vertical: 10),
                        child: Column(
                          children: [
                            buildTextFormField("Name", _nameController),
                            const SizedBox(height: 20),
                            buildTextFormField(
                                "Designation", _designationController),
                            const SizedBox(height: 70),
                            ElevatedButton(
                              onPressed: () async {
                                if (_formKey.currentState.validate()) {
                                  _bloc.add(EditEmployeeData(
                                      docId: widget.docId,
                                      name: _nameController.text,
                                      designation:
                                          _designationController.text));
                                }
                              },
                              style: ElevatedButton.styleFrom(
                                  primary: Colors.green,
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 30, vertical: 15),
                                  textStyle: const TextStyle(
                                      fontSize: 30,
                                      fontWeight: FontWeight.bold)),
                              child: const Text(
                                "Save",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Color.fromRGBO(255, 255, 255, 1),
                                    fontSize: 18),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  TextFormField buildTextFormField(label, inputController) {
    return TextFormField(
      controller: inputController,
      cursorColor: Colors.green,
      validator: (value) {
        return validateData(value, label, inputController);
      },
    );
  }

  String? validateData(value, label, inputController) {
    if (value.isEmpty) {
      return "Please Enter $label";
    }
    if (RegExp(r'[!@#<>?":_`~;[\]\\|=+)(*&^%0-9-]').hasMatch(value)) {
      return "Please enter valid name";
    }

    return null;
  }
}
