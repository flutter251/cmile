part of 'employee_edit_bloc.dart';

@immutable
abstract class EmployeeEditEvent {}

class EditEmployeeData extends EmployeeEditEvent{
  final String docId;
  final String name;
  final String designation;

  EditEmployeeData({required this.docId, required this.name,required this.designation});
}
