part of 'employee_edit_bloc.dart';

@immutable
abstract class EmployeeEditState {}

class EmployeeEditInitial extends EmployeeEditState {}

class OnEditState extends EmployeeEditState {
  final bool isEditSuccessful;
  OnEditState({required this.isEditSuccessful});
}
