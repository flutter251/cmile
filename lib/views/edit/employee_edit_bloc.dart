import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cmile_coding_assignment/repository/repository.dart';
import 'package:flutter/material.dart';
part 'employee_edit_event.dart';
part 'employee_edit_state.dart';

class EmployeeEditBloc extends Bloc<EmployeeEditEvent, EmployeeEditState> {
  Repository repository;
  EmployeeEditBloc({required this.repository}) : super(EmployeeEditInitial());

  @override
  Stream<EmployeeEditState> mapEventToState(EmployeeEditEvent event) async* {
    if (event is EditEmployeeData) {

      bool isEditSuccessful = await Repository().editEmployee(docId: event.docId, name: event.name, designation: event.designation);
      yield OnEditState(isEditSuccessful: isEditSuccessful);
    }
  }
}
