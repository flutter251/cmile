import 'package:cmile_coding_assignment/di/injection_container.dart' as di;
import 'package:cmile_coding_assignment/views/dashboard/dashboard_screen.dart';
import 'package:cmile_coding_assignment/views/login/login_screen.dart';
import 'package:cmile_coding_assignment/views/splashscreen/splash_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await di.init();
  runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Cmile',
      initialRoute: "/SplashScreen",
      routes: {
        '/SplashScreen': (context) => const SplashScreen(),
        '/DashboardScreen': (context) => const DashboardScreen(),
        '/LoginScreen': (context) => const LoginScreen(),
      }));
}
