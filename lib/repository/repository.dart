import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Repository {

  final FirebaseFirestore _firebaseFireStore = FirebaseFirestore.instance;

  static Future<SharedPreferences> getSharedPreferencesInstance() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs;
  }

  Future<bool> getUserLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isLoggedIn = prefs.getBool('user_logged_in') ?? false;
    return isLoggedIn;
  }

  Future<bool> setUserLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('user_logged_in', true);
    return true;
  }

  Future<bool> deleteEmployee({required String docId}) async{
    try{
      await _firebaseFireStore.collection("empData").doc(docId).delete();
      return true;
    }catch(e){
      return false;
    }
  }

  Future<bool> editEmployee({required String docId,required String name, required String designation}) async{
    await _firebaseFireStore
        .collection("empData")
        .doc(docId)
        .update({
      'empName': name,
      'designation': designation
    });
    return true;
  }
}
