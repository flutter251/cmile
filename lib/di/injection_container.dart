import 'package:cmile_coding_assignment/repository/repository.dart';
import 'package:cmile_coding_assignment/views/dashboard/dashboard_bloc.dart';
import 'package:cmile_coding_assignment/views/edit/employee_edit_bloc.dart';
import 'package:cmile_coding_assignment/views/login/login_bloc.dart';
import 'package:get_it/get_it.dart';

final injector = GetIt.instance;

Future<void> init() async {
  injector.registerLazySingleton<Repository>(() => Repository());
  injector.registerFactory<LoginBloc>(() => LoginBloc(repository: injector()));
  injector.registerFactory<DashboardBloc>(() => DashboardBloc(repository: injector()));
  injector.registerFactory<EmployeeEditBloc>(() => EmployeeEditBloc(repository: injector()));
}
